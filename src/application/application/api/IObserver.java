package application.application.api;

import application.application.fassade.State;

public interface IObserver {
	
	public void update(int state);
}
