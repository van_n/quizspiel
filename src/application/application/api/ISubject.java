package application.application.api;
import application.application.fassade.State;

public interface ISubject {
	
	public void attach(IObserver observer);
	
	public void detach(IObserver observer);
	
	public void notifyObserver(int state);
}
