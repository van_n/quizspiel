package application.application.api;
import application.application.domain.*;
import application.application.fassade.State;

public interface IUcControllerQuestionRound {
	
	public State kategorieAuswaehlen(Category category);
	
	public State antwortGeben(Answer answer);
	
}
