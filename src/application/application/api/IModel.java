package application.application.api;

import java.util.ArrayList;

import application.application.domain.Field;
import application.application.domain.Player;

public interface IModel extends ISubject{
	
	
	/**
	 * 
	 */
	public void startGame();
	
	/**
	 * 
	 */
	public void spielerzahlBestimmen(int anzahl);
	
	/**
	 * 
	 */
	public void wuerfeln();
	
	/**
	 * 
	 */
	public void wsAuswaehlen(int wsId);
	
	/**
	 * 
	 * @param catID
	 */
	public void kategorieAuswaehlen(int catID);
	
	/**
	 * 
	 * @param ansID
	 */
	public void antwortGeben(int ansID);
	
	
	/**
	 * Spieler, der am Zug ist, wird zurückgegeben.
	 * @return
	 */
	public Player getCurrentPlayer();
	
	/**
	 * Die gewürfelte AUgenzahl wird zurückgegeben.
	 * @return
	 */
	public int getRollNumber();
	
	/**
	 * Die Anzahl der Würfe im aktuellen Spielzug werden zurückgegeben.
	 * @return
	 */
	public int getNumberOfThrows();
	
	public Player[] getPlayers();
	
	public ArrayList<Integer> getPositionsOnPath(Player player);
	
	public ArrayList<Integer> getPositionsInHome(Player player);
}
