package application.application.fassade;

import java.util.ArrayList;

import application.application.api.IModel;
import application.application.api.IObserver;
import application.application.api.ISubject;
import application.application.domain.Field;
import application.application.domain.Game;
import application.application.domain.Player;

public class Fassade implements IModel{
	
	private ArrayList<IObserver> observers;
	
	private Game game;
	private State currentState;
	private UcControllerExecuteTurn uccET;
	
	public Fassade() {
		this.observers = new ArrayList<IObserver>();
		this.game = new Game();
		this.currentState = new State();
		this.uccET = new UcControllerExecuteTurn(this.game);
	}
	
	public void startGame() {
		if(currentState.getCurrentState() != State.START) {
			notifyObserver(State.ERROR);
		} else {
			currentState.setCurrentState(State.START);
			notifyObserver(currentState.getCurrentState());
		}
		
	}

	public void spielerzahlBestimmen(int numberOfPlayers) {
		if(currentState.getCurrentState() == State.START) {
			game.setPlayers(numberOfPlayers);
			this.currentState.setCurrentState(game.getState().getCurrentState());
			notifyObserver(this.currentState.getCurrentState());
		} else {
			notifyObserver(State.ERROR);
		}
	}

	public void wuerfeln() {
		// TODO Auto-generated method stub
		if(currentState.getCurrentState() == State.ROLL || currentState.getCurrentState() == State.ROLL_FAIL) {
			uccET.wuerfeln();
			this.currentState.setCurrentState(game.getState().getCurrentState());
			notifyObserver(this.currentState.getCurrentState());
		} else {
			notifyObserver(State.ERROR);
		}
		
	}

	public void wsAuswaehlen(int wsId) {
		// TODO Auto-generated method stub
		if(currentState.getCurrentState() == State.CHOOSEWS || currentState.getCurrentState() == State.IOOBE) {
			this.currentState.setCurrentState(uccET.wsAuswaehlen(wsId));
			notifyObserver(this.currentState.getCurrentState());
		} else {
			notifyObserver(State.ERROR);
		}
		
	}

	public void kategorieAuswaehlen(int catID) {
		// TODO Auto-generated method stub
		
	}

	public void antwortGeben(int ansID) {
		// TODO Auto-generated method stub
		
	}
	
	public Player[] getPlayers() {
		return game.getPlayers();
	}

	public Player getCurrentPlayer() {
		return game.getCurrentPlayer();
	}

	public int getRollNumber() {
		// TODO Auto-generated method stub
		return game.getRolledNumber();
	}

	public int getNumberOfThrows() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void notifyObserver(int state) {
		for(IObserver o : observers) {
			o.update(state);
		}
		
	}

	public void attach(IObserver observer) {
		observers.add(observer);
		
	}

	public void detach(IObserver observer) {
		observers.remove(observer);
		
	}

	public ArrayList<Integer> getPositionsOnPath(Player player) {
		// TODO Auto-generated method stub
		return this.game.getBoard().getFiguresOnPath(player);
	}

	public ArrayList<Integer> getPositionsInHome(Player player) {
		return this.game.getBoard().getFiguresInHome(player);
	}
}
