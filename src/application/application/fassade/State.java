package application.application.fassade;
public class State {
	
	public static final int START = 0;
	public static final int ROLL = 1;
	public static final int ROLL_FAIL = 2;
	public static final int NOT_VALID = 3;
	public static final int CHOOSEWS = 4;
	public static final int CHOOSECATEGORY = 5;
	public static final int ANSWER = 6;
	public static final int ERROR = 7;
	public static final int IOOBE = 8;
	
	private int currentState;

	public int getCurrentState() {
		return currentState;
	}

	public void setCurrentState(int currentState) {
		this.currentState = currentState;
	}
	
	
}
