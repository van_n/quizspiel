package application.application.fassade;

import application.application.domain.Game;

public class UcControllerExecuteTurn {

	private Game game;
	
	public UcControllerExecuteTurn(Game game) {
		this.game = game;
	}
	
	public int wsAuswaehlen(int wsId) {		
		// Message #1.1 to game:application.application.domain.Game
		game.selectWS(wsId);
		
		return game.getState().getCurrentState();
	}

	public int wuerfeln() {
		
		game.rollDice();
		
		return game.getState().getCurrentState();
	}
}
