package application.application.domain;
public class HomeField extends Field{
	
	private Player owner;
	
	public HomeField(Player owner) {
		this.owner = owner;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public WissensStreiter getWs() {
		return ws;
	}

	public void setWs(WissensStreiter ws) {
		this.ws = ws;
	}
	
	
	
}
