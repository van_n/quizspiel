package application.application.domain;
public enum FieldState {
	
	EMPTY, BLOCKED_BY_OWN, BLOCKED_BY_RIVAL
	
}
