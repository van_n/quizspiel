package application.application.domain;

import java.util.ArrayList;

public class Board {

	private final static int NUMBER_OF_FIELDS = 48;
	private final static int NUMBER_OF_HOMEFIELDS = 4;

	/**
	 * Pfad auf dem sich die Figuren bewegen
	 */
	private Field[] path;
	
	/**
	 * 
	 */
	private Field[][] homefields;
	
	/**
	 * Startfelder der entsprechenden Spieler
	 */
	private Field[] startfields;

	public Board(Player[] players) {
		buildPath(players);
		buildHomes(players);
	}

	/**
	 * Der Pfad wird besteht aus 48 Feldern, wobei jedes 12 Feld ein Startfeld
	 * mit einem Spieler als Besitzer ist.
	 * 
	 * @param players
	 */
	private void buildPath(Player[] players) {

		int playerIndex = 0;
		path = new Field[NUMBER_OF_FIELDS];
		startfields = new Field[players.length];

		for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
			if ((i % 12) == 0 && playerIndex < players.length) {
				path[i] = new StartField(players[playerIndex]);
				startfields[playerIndex] = path[i];
				playerIndex++;
			} else {
				path[i] = new NormalField();
			}
		}

	}

	/**
	 * Die Heimatfelder werden den Spielern zugeordnet. Die Figuren werden initialisiert
	 * 
	 * @param players
	 */
	private void buildHomes(Player[] players) {

		homefields = new Field[players.length][NUMBER_OF_HOMEFIELDS];

		for (int i = 0; i < players.length; i++) {
			for (int j = 0; j < NUMBER_OF_HOMEFIELDS; j++) {
				homefields[i][j] = new HomeField(players[i]);
				homefields[i][j].setWs(new WissensStreiter(players[i], j));
			}
		}
	}

	/**
	 * Prüft, ob sich auf dem eigenen Heimatfeldern eine Figur steht
	 * 
	 * @param currentPlayer
	 * @return
	 */
	public boolean hasInActiveWs(Player currentPlayer) {

		Field[] home = homefields[currentPlayer.getId()];

		for (int i = 0; i < NUMBER_OF_HOMEFIELDS; i++) {
			if (home[i].isBesetzt()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Prüft, ob auf den eigenen Heimatfeldern eine Figur feld
	 * 
	 * @param currentPlayer
	 * @return
	 */
	public boolean hasActiveWs(Player currentPlayer) {

		Field[] home = homefields[currentPlayer.getId()];

		for (int i = 0; i < NUMBER_OF_HOMEFIELDS; i++) {
			if (!(home[i].isBesetzt())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Figur des aktuellen Spielers wird auf das Startfeld gesetzt
	 * 
	 * @param player
	 */
	public void activateWS(Player player) {

		int playerId = player.getId();

		for (int i = 0; i < NUMBER_OF_HOMEFIELDS; i++) {
			if (homefields[playerId][i].isBesetzt()) {
				startfields[playerId].setWs(homefields[playerId][i].getWs());
				homefields[playerId][i].setWs(null);
				break;
			}
		}
	}

	/**
	 * Die gewählte Figur wird zurück auf das Heimatfeld gesetzt
	 * 
	 * @param player
	 * @param wsID
	 */
	public void deactivateWS(Player player, int wsID) {
		ArrayList<Integer> figuresOnPath = getFiguresOnPath(player);
		WissensStreiter ws = path[figuresOnPath.get(wsID)].getWs();
		homefields[player.getId()][wsID].setWs(ws);
		path[figuresOnPath.get(wsID)].setWs(null);
	}

	/**
	 * Die Figuren eines Sielers auf dem Pfad werden erfasst
	 * @param player
	 * @return
	 */
	public ArrayList<Integer> getFiguresOnPath(Player player) {
		ArrayList<Integer> figures = new ArrayList<Integer>();
		for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
			if (path[i].isBesetzt() && path[i].getWs().getOwner() == player) {
				figures.add(i);
			}
		}
		return figures;
	}

	/**
	 * Figuren eines Spielers in der Heimat werden erfasst
	 * @param player
	 * @return
	 */
	public ArrayList<Integer> getFiguresInHome(Player player) {
		ArrayList<Integer> figures = new ArrayList<Integer>();
		for (int i = 0; i < NUMBER_OF_HOMEFIELDS; i++) {
			if (homefields[player.getId()][i].isBesetzt()) {
				figures.add(i);
			}
		}
		return figures;
	}

	/**
	 * Prüft, ob das Startfield blockiert ist, und gegebenfalls von wem
	 * @param player
	 * @return
	 */
	public FieldState isStartfieldBlocked(Player player) {
		
		int playerId = player.getId();
		
		if (startfields[playerId].isBesetzt()) {
			if (startfields[playerId].getWs().getOwner().equals(player)) {
				return FieldState.BLOCKED_BY_OWN;
			}
			return FieldState.BLOCKED_BY_RIVAL;
		}

		return FieldState.EMPTY;
	}

	/**
	 * Prüft, ob sich die gewählte Figur um die gegebene Augenzahl bewegen kann
	 * 
	 * @param player
	 * @param wsId
	 * @param augenzahl
	 * @return
	 */
	public FieldState movable(Player player, int wsId, int augenzahl) {
		ArrayList<Integer> figuresOnPath = getFiguresOnPath(player);
		int selectedField = figuresOnPath.get(wsId);

		if (get(selectedField + augenzahl).isBesetzt()) {
			if (get(selectedField + augenzahl).getWs().getOwner()
					.equals(player)) {
				return FieldState.BLOCKED_BY_OWN;
			}
			return FieldState.BLOCKED_BY_RIVAL;
		}
		return FieldState.EMPTY;
	}
	
	
	/**
	 * Bewegt eine Spielfigur
	 * 
	 * @param player
	 * @param wsId
	 * @param augenzahl
	 */
	public void move(Player player, int wsId, int augenzahl) {
		ArrayList<Integer> figuresOnPath = getFiguresOnPath(player);
		
		int selectedField = figuresOnPath.get(wsId);

		WissensStreiter ws = path[selectedField].getWs();
		path[selectedField].setWs(null);
		path[selectedField + augenzahl].setWs(ws);
	}

	/**
	 * Gibt ein Feld auf dem Pfad aus. Hier wird der logische Kreis gebildet
	 * @param index
	 * @return
	 */
	public Field get(int index) {
		return path[index % NUMBER_OF_FIELDS];
	}

	public Field[] getPath() {
		return path;
	}

	public void setPath(Field[] path) {
		this.path = path;
	}

	public Field[][] getHomes() {
		return homefields;
	}

	public void setHomes(Field[][] homes) {
		this.homefields = homes;
	}

}
