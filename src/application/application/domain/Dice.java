package application.application.domain;

public class Dice {

	
	/**
	 * Erzeugt eine Zahl zwischen 1 und 6
	 * @return
	 */
	public int roll() {

		int number = 0;

		do {
			number = (int) Math.round(Math.random() * 10);
		} while (number <= 0 || number > 6);

		return number;
	}
}
