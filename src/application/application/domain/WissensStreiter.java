package application.application.domain;

public class WissensStreiter {
	
	private Player owner;
	private int id;
	
	public WissensStreiter(Player player, int id) {
		this.id = id;
		this.owner = player;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
