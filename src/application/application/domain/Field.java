package application.application.domain;
public abstract class Field {
	
	protected WissensStreiter ws;
	
	public Field() {
		this.ws = null;
	}
	
	public boolean isBesetzt() {
		if(this.ws == null) {
			return false;
		}
		
		return true;
	}

	public WissensStreiter getWs() {
		return ws;
	}

	public void setWs(WissensStreiter ws) {
		this.ws = ws;
	}
	
	
}
