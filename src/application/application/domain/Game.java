package application.application.domain;

import java.util.ArrayList;

import application.application.fassade.State;

public class Game {
	
	/**
	 * Die zuletzt gerollte Augenzahl
	 */
	private int rolledNumber;
	
	/**
	 * Anzahl der Würfe des aktuellen Spielers
	 * Wird nach jedem Zug auf drei gesetzt
	 */
	private int numberOfRolls = 3;
	
	/**
	 * Spieler, wobei der Index mit der ID der Spieler übereinstimmt
	 */
	private Player[] players;
	private Dice dice;
	private Board board;
	
	private State state;
	
	/**
	 * Aktueller Spieler
	 */
	private Player currentPlayer;
	
	public Game() {
		dice = new Dice();
		state = new State();
	}
	
	public void selectWS() {
		return;
	}

	/**
	 * Die Anzahl der Spieler wird initialisiert
	 * @param numberOfPlayers
	 */
	public void setPlayers(int numberOfPlayers) {
		players = new Player[numberOfPlayers];
		for(int i = 0; i < numberOfPlayers; i++) {
			players[i] = new Player(i);
		}
		currentPlayer = players[0];
		
		board = new Board(players);
		
		state.setCurrentState(State.ROLL);
	}
	


	public int getRolledNumber() {
		return rolledNumber;
	}

	public void rollDice() {
		
		this.rolledNumber = dice.roll();
		numberOfRolls--;
		boolean inactiveWS = board.hasInActiveWs(currentPlayer);
		boolean activeWS = board.hasActiveWs(currentPlayer);
		
		if(rolledNumber == 6 && inactiveWS) {
			
			FieldState startFieldState = board.isStartfieldBlocked(currentPlayer);
			
			if(startFieldState == FieldState.EMPTY) {
				board.activateWS(currentPlayer);
				nextPlayer();
				state.setCurrentState(State.ROLL);
			}
			else if(startFieldState == FieldState.BLOCKED_BY_OWN) {
				state.setCurrentState(State.CHOOSEWS);
			}
			else{
				state.setCurrentState(State.CHOOSECATEGORY);
			}
		}
		else if((rolledNumber != 6 && activeWS) || (rolledNumber == 6 && !inactiveWS)) {
			state.setCurrentState(State.CHOOSEWS);
		}
		else if(rolledNumber != 6 && !activeWS) {
			if(numberOfRolls > 0) {
				state.setCurrentState(State.ROLL_FAIL);
			}
			else{
				nextPlayer();
				state.setCurrentState(State.ROLL);
			}
		}
	}
	
	public void selectWS(int wsId) {
		
		ArrayList<Integer> figuresOfPlayers = board.getFiguresOnPath(currentPlayer);
		if(wsId >= figuresOfPlayers.size()) {
			state.setCurrentState(State.IOOBE);
		}
		else {
			FieldState isMovable = board.movable(currentPlayer, wsId, rolledNumber);
			
			if(isMovable == FieldState.EMPTY) {
				board.move(currentPlayer, wsId, rolledNumber);
				nextPlayer();
				state.setCurrentState(State.ROLL);
			}
			else if(isMovable == FieldState.BLOCKED_BY_OWN) {
				state.setCurrentState(State.CHOOSEWS);
			}
			else {
				state.setCurrentState(State.CHOOSECATEGORY);
			}
		}
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public Player[] getPlayers() {
		return players;
	}
	
	public Board getBoard() {
		return this.board;
	}
	
	public void nextPlayer() {
		int index = currentPlayer.getId();
		numberOfRolls = 3;
		if(index + 1 >= players.length) {
			currentPlayer = players[0];
		} else {
			currentPlayer = players[index + 1];
		}
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	
	
}
