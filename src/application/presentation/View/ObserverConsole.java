package application.presentation.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import application.application.api.IModel;
import application.application.api.IObserver;
import application.application.fassade.Fassade;
import application.application.fassade.State;
import java.io.*;

public class ObserverConsole implements IObserver {

	private IModel model;

	private Semaphore sem = new Semaphore(0);
	private Map<String, Integer> call = new HashMap<String, Integer>();

	public ObserverConsole(IModel model) {
		this.call.put("s", 0);
		this.call.put("2s", 1);
		this.call.put("3s", 2);
		this.call.put("4s", 3);
		this.call.put("w", 4);
		this.call.put("0", 5);
		this.call.put("1", 6);
		this.call.put("2", 7);
		this.call.put("3", 8);
		
		this.model = model;
		this.model.attach(this);

		initComponents();
	}

	public void initComponents() {
		getInput();
	}

	
	
	public void update(int state) {

		if(state == State.START) {
			System.out.println("Bitte wählen Sie die gewünschte Anzahl von Spielern aus: '2s', '3s', '4s'");
		}
		
		if(state == State.ROLL) {
			for(int i = 0; i < model.getPlayers().length; i++) {
				System.out.print("Player " + i + ": ");
				System.out.print("Heimat " + this.model.getPositionsInHome(this.model.getPlayers()[i]) + " ");
				//System.out.println("Pfad " + this.model.getPositionsOnPath(this.model.getPlayers()[i]));
				ArrayList<Integer> pathFigures = this.model.getPositionsOnPath(this.model.getPlayers()[i]);
				
				if(pathFigures.size() != 0) {
					System.out.print("[Figur]:[Feld] s");
					for(int j = 0; j < pathFigures.size(); j++) {
						System.out.print(j + ":" + pathFigures.get(j) + " ");
					}
				}
				System.out.println();
			}
			
			System.out.println();
			System.out.println("Zuletzt gewürfelt: " + this.model.getRollNumber());
			System.out.println();
			
			System.out.println("Spieler " + model.getCurrentPlayer().getId() + " ist am Zug.");
			System.out.println("Drücke w zum Würfeln.");
		}
		
		if(state == State.ROLL_FAIL) {
			System.out.println("Zuletzt gewürfelt: " + this.model.getRollNumber());
			System.out.println("Spieler " + model.getCurrentPlayer().getId() + " nocheinmal würfeln!");
		}
		
		if(state == State.NOT_VALID) {
			System.out.println("Zug nicht möglich! Bitte wählen Sie eine andere Figur");
		}
		
		if(state == State.CHOOSEWS) {
			System.out.println("Zuletzt gewürfelt: " + model.getRollNumber());
			System.out.println("Bitte Wissensstreiter auswählen!");
		}
		
		if(state == State.CHOOSECATEGORY) {
			System.out.println("CHOOSECATEGORY");
		}
		
		if(state == State.ERROR) {
			System.out.println("ERROR");
		}
		
		if(state == State.IOOBE) {
			System.out.println("Diese Figur befindet sich nicht auf dem Pfad! Bitte wählen Sie eine andere Figur");
		}

		sem.release();
		
	}

	private void getInput() {
		String str = new String();
		while (true) {
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				str = in.readLine();
			} catch (IOException e) {
			}
			int i = (this.call.get(str) != null ? this.call.get(str) : 0);
			switch (i) {
			
			case 0: {
				new Thread() {
					public void run() {
						// System.out.println("Thread: " + getName() + " running");
						model.startGame();
					}
				}.start();
			};
				break;
				
			case 1: {
				new Thread() {
					public void run() {
						// System.out.println("Thread:" + getName() + " running");
						model.spielerzahlBestimmen(2);
					}
				}.start();
			};
				break;
				
			case 2: {
				new Thread() {
					public void run() {
						// System.out.println("Thread: " + getName() + " running");
						model.spielerzahlBestimmen(3);
					}
				}.start();
			};
				break;
				
			case 3: {
				new Thread() {
					public void run() {
						// System.out.println("Thread: " + getName() + " running");
						model.spielerzahlBestimmen(4);
					}
				}.start();
			};
				break;
				
			case 4: {
				new Thread() {
					public void run() {
						// System.out.println("Thread: " + getName() + " running");
						model.wuerfeln();
					}
				}.start();
			};
					break;
					
			case 5: {
				new Thread() {
					public void run() {
						model.wsAuswaehlen(0);
					}
				}.start();
			};
				break;
				
			case 6: {
				new Thread() {
					public void run() {
						model.wsAuswaehlen(1);
					}
				}.start();
			};
				break;	
				
			case 7: {
				new Thread() {
					public void run() {
						model.wsAuswaehlen(2);
					}
				}.start();
			};
				break;	
				
			case 8: {
				new Thread() {
					public void run() {
						model.wsAuswaehlen(3);
					}
				}.start();
			};
				break;	

			default:
				this.update(State.ERROR);
				break;
			}

			try {
				sem.acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		IModel model = new Fassade();
		ObserverConsole observerConsole = new ObserverConsole(model);
	}
}